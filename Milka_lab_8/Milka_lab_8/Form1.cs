﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milka_lab_8
{
    public partial class Form1 : Form
    {

        MilkaEntities1 DB = new MilkaEntities1();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dgv.DataSource = DB.Customer.ToList();
        }
        private void add_Click(object sender, EventArgs e)
        {
            try
            {
                if(name.Text.Replace(" ", "") == "" || surname.Text.Replace(" ","") == "" || adress.Text.Replace(" ", "") == "")
                {
                    throw new ArgumentException("Fill all information.");
                }
                DB.Add_Customer(name.Text, surname.Text, adress.Text);
                MessageBox.Show("Customer add!");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Error with add. " + ex.Message);
            }
            dgv.DataSource = DB.Customer.ToList();
        }

       
    }
}
